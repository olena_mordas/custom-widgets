return Widgets.Static.extend({
	//defines the element into which the custom template is rendered
    customEl: '.widget-content',
    renderContent: _.noop,
    
    
    	
    	
    _timer: null,
 
    afterInitialize: function () {
        this.updateTime();
    },
 
    updateTime: function () {
    	console.log('inside the updateTime');
    	debugger;
        if (this._timer) {
            clearTimeout(this._timer);
        }
        var now = new Date();
        this.$('.js-clock').text(now.toLocaleTimeString());
 
        this._timer = setTimeout(this.bind(this.updateTime), 500);
    },
    getCustomTemplateParams: function(){
    	return{
    		now: new Date().toISOString(),
    		//if 'greeting' is not defined in the model the default d be 'Hello!' 
    		greeting: this.model.get('greeting') || 'Hello!',
    		
    	};
    }
    
});